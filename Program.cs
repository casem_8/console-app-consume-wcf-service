﻿using System;
using CalculadoraServiceReference;
using ProbandoServicio;


namespace ProbandoServicio
{
    class Program
    {
        static async Task Main(string[] args)
        {
            // Creamos una instancia del cliente 
            CalculadoraClient client = new CalculadoraClient();

            // Solicitamos al cliente los numeros
            Console.WriteLine("Ingrese el primer número:");
            int numero1 = int.Parse(Console.ReadLine());

            Console.WriteLine("Ingrese el segundo número:");
            int numero2 = int.Parse(Console.ReadLine());

            // Llamamos a la operación sumar del servicio
            int resultado = await client.SumarAsync(numero1, numero2);

            // Resultado
            Console.WriteLine($"La suma es: {resultado}");

            // Cerrar la comunicación con el servicio
            client.Close();
        }
    }
}
